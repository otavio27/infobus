#!/usr/bin/env bash

source Onibus-Bot.sh

ShellBot.init --token 'Seu token' --return map --monitor
#======================================================================================================================#
TimeData_func() {

    data=$(date +%d/%m/%Y)
    hora=$(date +%H:%M)

    TIME=$(date +%H)

    if [[ "${TIME}" = "00" ]]; then
        TIME="24"
    else
        TIME="${TIME}"
    fi
}

Voltar_func() {
    
    BTV=''
    
    ShellBot.InlineKeyboardButton --button 'BTV' --text 'VOLTAR' --callback_data '/start' --line 1
    
    ShellBot.sendMessage --chat_id ${callback_query_message_chat_id[$id]} \
    --text 'VOLTAR PRESSIONE -> /terminais' --parse_mode markdown
    
}

Message_func() {
    
    local MSG
    local MSK
    TimeData_func
    
    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${callback_query_data}" | sort | uniq)
    MSG="*LINHA*: ${Linha/,/ - }\n"
    MSG+="*Data*: ${data} *Hora*: ${hora}\n"
    MSG+="\n*Dias úteis*: - Ida\n"
    if [[ $(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[0].time_data[][].departure_time') ]]; then
        MSG+=$(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[0].time_data[][].departure_time' | sort | uniq)
    else
        MSG+="⚠️ Esta linha não roda neste dia!"
    fi
    
    MSG+="\n\n*Dias úteis*: - Volta\n"
    if [[ $(cat Linhas/${callback_query_data} | jq -r '.[1].stop_data[].service_data[0].time_data[][].departure_time') ]]; then
        MSG+=$(cat Linhas/${callback_query_data} | jq -r '.[1].stop_data[].service_data[0].time_data[][].departure_time' | sort | uniq)
    else
        MSG+="⚠️ Esta linha não roda neste dia!"
    fi
    
    MSK="\n\n*Sábado*: - Ida\n"
    if [[ $(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time') ]]; then
        MSK+=$(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time' | sort | uniq)
    else
        MSK+="⚠️ Esta linha não roda neste dia!"
    fi
    
    MSK+="\n\n*Sábado*: - Volta\n"
    if [[ $(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time') ]]; then
        MSK+=$(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time' | sort | uniq)
    else
        MSK+="⚠️ Esta linha não roda neste dia!"
    fi
    
    MSK+="\n\n*Domingo*: - Ida\n"
    if [[ $(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[2].time_data[][].departure_time') ]]; then
        MSK+=$(cat Linhas/${callback_query_data} | jq -r '.[0].stop_data[].service_data[2].time_data[][].departure_time' | sort | uniq)
    else
        MSK+="⚠️ Esta linha não roda neste dia!"
    fi
    
    MSK+="\n\n*Domingo*: - Volta\n"
    if [[ $(cat Linhas/${callback_query_data} | jq -r '.[1].stop_data[].service_data[2].time_data[][].departure_time') ]]; then
        MSK+=$(cat Linhas/${callback_query_data} | jq -r '.[1].stop_data[].service_data[2].time_data[][].departure_time'| sort | uniq)
    else
        MSK+="⚠️ Esta linha não roda neste dia!"
    fi
    
    ShellBot.editMessageText \
    --message_id ${callback_query_message_message_id[$id]} \
    --chat_id ${callback_query_message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
    
    ShellBot.sendMessage --chat_id ${callback_query_from_id[$id]} \
    --text "$(echo -e ${MSK})" --parse_mode markdown
    
    Voltar_func
}

Linha0040_func() {
    
    local MSJ
    local MSG
    local MSK
    TimeData_func
    
    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${callback_query_data}" | sort | uniq)
    MSG="*LINHA*: ${Linha/,/ - }\n"
    MSG+="*Data*: ${data} *Hora*: ${hora}\n"
    MSG+="\n*Partidas Terminal Norte*\n*Ida para Terminal Tupy*\n*Dias Úteis:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[0].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Sábados:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[0].service_data[1].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Domingos:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[0].service_data[2].time_data[][].departure_time' | sort | uniq)
    
    MSH="\n\n*Partidas Terminal Central*\n*Ida para Terminal Tupy*\n*Sábados:*\n"
    MSH+=$(cat Linhas/0040 | jq -r '.[0].stop_data[1].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSH+="\n\n*Domingos:*\n"
    MSH+=$(cat Linhas/0040 | jq -r '.[0].stop_data[1].service_data[1].time_data[][].departure_time' | sort | uniq)
    
    MSJ="\n\n*Partidas Terminal Tupy*\n*Volta para Terminal Norte*\n*Dias Úteis:*\n"
    MSJ+=$(cat Linhas/0040 | jq -r '.[1].stop_data[0].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSJ+="\n\n*Sábados:*\n"
    MSJ+=$(cat Linhas/0040 | jq -r '.[1].stop_data[0].service_data[1].time_data[][].departure_time' | sort | uniq)
    MSJ+="\n\n*Domingos:*\n"
    MSJ+=$(cat Linhas/0040 | jq -r '.[1].stop_data[0].service_data[2].time_data[][].departure_time' | sort | uniq)
    
    MSK+="\n\n*Partidas Terminal Central*\n*Volta para Terminal Norte*\n*Sábados:*\n"
    MSK+=$(cat Linhas/0040 | jq -r '.[1].stop_data[1].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSK+="\n\n*Domingos:*\n"
    MSK+=$(cat Linhas/0040 | jq -r '.[1].stop_data[1].service_data[1].time_data[][].departure_time' | sort | uniq)
    
    ShellBot.editMessageText \
    --message_id ${callback_query_message_message_id[$id]} \
    --chat_id ${callback_query_message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
    
    ShellBot.sendMessage --chat_id ${callback_query_from_id[$id]} \
    --text "$(echo -e ${MSH})" --parse_mode markdown
    
    ShellBot.sendMessage --chat_id ${callback_query_from_id[$id]} \
    --text "$(echo -e ${MSJ})" --parse_mode markdown
    
    ShellBot.sendMessage --chat_id ${callback_query_from_id[$id]} \
    --text "$(echo -e ${MSK})" --parse_mode markdown
    
    Voltar_func
}

Verification_func() {
    
    if [[ "${callback_query_data}" = "0040" ]]; then
        Linha0040_func
    else
        Message_func
    fi
}

Linhas_func() {
    
    local MSG
    
    MSG="*ESCOLHA UMA DAS LINHAS*"
    L=1
    BTS=''
    while IFS=',' read route_id route_long_name; do
        [[ $C -eq 1 ]] && { C=1; let L=$L+1; } || { let C=$C+1; }
        ShellBot.InlineKeyboardButton --button 'BTS' --text "${route_id} ${route_long_name%%,*}" --callback_data "${route_id}" --line ${L}
    done < <(jq -r '.[].routes[] | "\(.route_id),\(.route_long_name),\(.station_name)"' ~/InfoBus/Routers/router.txt | grep "${callback_query_data}" | sort | uniq)
    
    ShellBot.editMessageText \
    --message_id ${callback_query_message_message_id[$id]} \
    --chat_id ${callback_query_message_chat_id[$id]} \
    --reply_markup "$(ShellBot.InlineKeyboardMarkup --button 'BTS')" \
    --text "$(echo -e ${MSG})" --parse_mode markdown
}

Terminais_func() {
    
    if [[ ! -e Users/${message_chat_first_name}.txt ]]; then
        echo -e "\nUser: ${message_chat_first_name}\nData: $(date +%D)\nHora: $(date +%T)" >> Users/${message_chat_first_name}.txt
    fi
    
    local MSG
    
    MSG="*ESCOLHA UM DOS TERMINAIS*"
    L=1
    BTS=''
    if [[ ! -e Routers/router.txt ]]; then

        curl -X GET https://onibus.info/api/routes/group \
        -H 'Accept: application/json, text/plain, /' \
        -H 'Accept-Encoding: gzip, deflate, br' \
        -H 'Accept-Language: en-US,en;q=0.5' \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive'  \
        -H 'Cookie: _ga=GA1.2.1013964565.1582001733; _gid=GA1.2.528327386.1582001733; _gat=1' \
        -H 'Host: onibus.info' -H 'If-None-Match: W/"163-eOdyfXpHh2eKTCvQmg+OnnpiK/g"' \
        -H 'Postman-Token: cc8a1153-53ff-4a95-8d29-efdcc58ed728,777c9fe8-5073-4dee-805b-d1d2a9a28507' \
        -H 'Referer: https://onibus.info/api/routes/group' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:68.0) Gecko/20100101 Firefox/68.0' \
        -H 'cache-control: no-cache' | gunzip >> Routers/router.txt
        
        while IFS=',' read station_name; do
            [[ $C -eq 2 ]] && { C=1; let L=$L+1; } || { let C=$C+1; }
            ShellBot.InlineKeyboardButton --button 'BTS' --text "${station_name}" --callback_data "${station_name}" --line ${L}
        done < <(jq -r '.[].routes[] | "\(.station_name)"' ~/InfoBus/Routers/router.txt | sort | uniq)
        
        ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
        --text "$(echo -e ${MSG})" --reply_markup \
        "$(ShellBot.InlineKeyboardMarkup --button 'BTS')" --parse_mode markdown
    else
        while IFS=',' read station_name; do
            [[ $C -eq 2 ]] && { C=1; let L=$L+1; } || { let C=$C+1; }
            ShellBot.InlineKeyboardButton --button 'BTS' --text "${station_name}" --callback_data "${station_name}" --line ${L}
        done < <(jq -r '.[].routes[] | "\(.station_name)"' ~/InfoBus/Routers/router.txt | sort | uniq)
        
        ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
        --text '*ESCOLHA UM DOS TERMINAIS*' --reply_markup \
        "$(ShellBot.InlineKeyboardMarkup --button 'BTS')" --parse_mode markdown
    fi
}

Help_func() {
    
    if [[ ! -e Users/${message_chat_first_name}.txt ]]; then
        echo -e "\nUser: ${message_chat_first_name}\nData: $(date +%D)\nHora: $(date +%T)" > Users/${message_chat_first_name}.txt
    fi
    
    local MSG
    
    MSG="🤖 Olá *${message_chat_first_name}!*\nSeja bem vindo ao seu novo assistente.\n"
    MSG+="\nPara usufruir dos serviços que o *InfoBus* oferece, digite:\n"
    MSG+="*/terminais* Para começar.\n"
    MSG+="Ou vá até *[/]* e clique em */terminais*.\n"
    MSG+="Será apresentado uma lista dos terminais da cidade.\n"
    MSG+="Após, só escolher o terminal desajado.\n"
    MSG+="Escolhendo o terminal.\n"
    MSG+="Será apresentado uma nova lista, com as respectivas linhas desse terminal.\n"
    MSG+="Você Também pode procurar somente pela linha.\n"
    MSG+="Para pesquisar somente pela linha, é preciso passar o número da mesma.\n"
    MSG+="*Exemplo:* 0040 0300\nDigite o número direto na barra de pesquisa.\n"
    MSG+="O método de consulta pelo horário da linha\ntraz somente os horários do dia da semana.\n"
    MSG+="tomando como base o horário e data da solicitação.\n"
    MSG+="Para uma consulta mais detalhada da linha\nbusque através dos terminais.\n"
    MSG+="\n⚠️\n"
    MSG+="Todas as informações obtidas pelo *InfoBus*.\n"
    MSG+="são em tempo real, devido a isso, sua consulta e retorno.\n"
    MSG+="dos resultados depende de conexão com a internet.\n"
    MSG+="podendo haver um pequeno delay.\n"
    MSG+="\n\nPara sugestões, críticas ou relatar problemas.\n"
    MSG+="Contate-nos através do e-mail:\n"
    MSG+="owtechdeveloper@gmail.com"
    
    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
    
}

while :; do
    
    # Obtem as atualizações
    ShellBot.getUpdates --limit 100 --offset $(ShellBot.OffsetNext) --timeout 30
    
    # Lista as atualizações.
    (
        for id in $(ShellBot.ListUpdates); do
            
            case ${message_text[$id]} in
                /help)
                Help_func ;;

                /start)
                Help_func ;;

                /terminais)
                Terminais_func ;;

                [0-9][0-9][0-9][0-9])
                source Horas.sh ;;
                *) ;;
            esac
            # Aqui somente são usados os valores calback
            case ${callback_query_data[$id]} in
                [A-Za-z]*)
                Linhas_func ;;
                
                [0-9][0-9][0-9][0-9])
                Verification_func ;;
            esac
        done
    ) &
done
