#!/usr/bin/env bash

TimeData_func() {

    data=$(date +%d/%m/%Y)
    DDS=$(date +%A)
    hora=$(date +%H:%M)

    TIME=$(date +%H)

    if [[ "${TIME}" = "00" ]]; then
        TIME="24"
    else
        TIME="${TIME}"
    fi
}

Dias_uteis_func() {

    local MSG
    local Linha
    TimeData_func

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${message_text}" | sort | uniq)
    MSG="*HORÁRIOS DO DIA DA SEMANA*\n"
    MSG+="*Data:* ${data} *Hora*: ${hora}"
    MSG+="\n*LINHA:* ${Linha/,/ - }\n"
    MSG+="\n*${DDS}:* >>> Ida\n"
    if [[ $(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[0].time_data[][].departure_time' | sort | uniq) ]]; then
        MSG+=$(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[0].time_data[][].departure_time' | sort | uniq)
    else
        MSG+="⚠️ Esta linha não roda neste dia!"
    fi
    MSG+="\n\n*${DDS}:* <<< Volta\n"
    if [[ $(cat Linhas/${message_text} | jq -r '.[1].stop_data[].service_data[0].time_data[][].departure_time' | sort | uniq) ]]; then
        MSG+=$(cat Linhas/${message_text} | jq -r '.[1].stop_data[].service_data[0].time_data[][].departure_time' | sort | uniq)
    else
        MSG+="⚠️ Esta linha não roda neste dia!"
    fi

    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
}

Sabados_func() {

    local MSK
    local Linha
    TimeData_func

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${message_text}" | sort | uniq)
    MSK="*HORÁRIOS DO DIA DA SEMANA*\n"
    MSK+="*Data:* ${data} *Hora*: ${hora}"
    MSK+="\n*LINHA:* ${Linha/,/ - }\n"
    MSK+="\n\n*${DDS}:* >>> Ida\n"
    if [[ $(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time' | sort | uniq) ]]; then
        MSK+=$(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time' | sort | uniq)
    else
        MSK+="⚠️ Esta linha não roda neste dia!"
    fi
    MSK+="\n\n*${DDS}:* <<< Volta\n"
    if [[ $(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time' | sort | uniq) ]]; then
        MSK+=$(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[1].time_data[][].departure_time' | sort | uniq)
    else
        MSK+="⚠️ Esta linha não roda neste dia!"
    fi

    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSK})" --parse_mode markdown
}

Domingos_func() {

    local MSJ
    local Linha
    TimeData_func

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${message_text}" | sort | uniq)
    MSJ="*HORÁRIOS DO DIA DA SEMANA*\n"
    MSJ+="*Data:* ${data} *Hora*: ${hora}"
    MSJ+="\n*LINHA:* ${Linha/,/ - }\n"
    MSJ+="\n\n*${DDS}:* >>> Ida\n"
    if [[ $(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[2].time_data[][].departure_time' | sort | uniq) ]]; then
        MSJ+=$(cat Linhas/${message_text} | jq -r '.[0].stop_data[].service_data[2].time_data[][].departure_time' | sort | uniq)
    else
        MSJ+="⚠️ Esta linha não roda neste dia!"
    fi
    MSJ+="\n\n*${DDS}:* <<< Volta\n"
    if [[ $(cat Linhas/${message_text} | jq -r '.[1].stop_data[].service_data[2].time_data[][].departure_time' | sort | uniq) ]]; then
        MSJ+=$(cat Linhas/${message_text} | jq -r '.[1].stop_data[].service_data[2].time_data[][].departure_time' | sort | uniq)
    else
        MSJ+="⚠️ Esta linha não roda neste dia!"
    fi

    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSJ})" --parse_mode markdown
}

Uteis0040_func() {

    local MSG
    local Linha
    TimeData_func

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${message_text}" | sort | uniq)

    MSG="*HORÁRIOS DO DIA DA SEMANA*\n"
    MSG+="*Data:* ${data} *Hora*: ${hora}"
    MSG+="\n*LINHA:* ${Linha/,/ - }\n"
    MSG+="\n*Partidas Terminal Norte*\n*Ida para Terminal Tupy*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[0].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Partidas Terminal Tupy*\n*Volta para Terminal Norte*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[1].stop_data[0].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n⚠️  *ALERTA*  ⚠️"
    MSG+="\nA linha 0040 - Tupy / Norte, no intervalo de 10h às 19h nos dias úteis"
    MSG+="\nestá sendo atendida pelas linhas 0041 - Norte / Centro e 0043 - Tupy / Centro."
    MSG+="\nNos demais horários o serviço segue normalmente."

    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
}

Sabados0040_func() {

    local MSG
    local Linha
    TimeData_func

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${message_text}" | sort | uniq)

    MSG="*HORÁRIOS DO DIA DA SEMANA*\n"
    MSG+="*Data:* ${data} *Hora*: ${hora}"
    MSG+="\n*LINHA:* ${Linha/,/ - }\n"
    MSG+="\n*Partidas Terminal Norte*\n*Ida para Terminal Tupy*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[0].service_data[1].time_data[][].departure_time' | sort | uniq)  
    MSG+="\n\n*Partidas Terminal Central*\n*Ida para Terminal Tupy*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[1].service_data[0].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Partidas Terminal Tupy*\n*Volta para Terminal Norte*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[1].stop_data[0].service_data[1].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Partidas Terminal Central*\n*Volta para Terminal Norte*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[1].stop_data[1].service_data[0].time_data[][].departure_time' | sort | uniq)

    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
}

Domingos0040_func() {

    local MSG
    local Linha
    TimeData_func

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id),\(.route_long_name)"' | grep "${message_text}" | sort | uniq)

    MSG="*HORÁRIOS DO DIA DA SEMANA*\n"
    MSG+="*Data:* ${data} *Hora*: ${hora}"
    MSG+="\n*LINHA:* ${Linha/,/ - }\n"
    MSG+="\n*Partidas Terminal Norte*\n*Ida para Terminal Tupy*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[0].service_data[2].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Partidas Terminal Central*\n*Ida para Terminal Tupy*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[0].stop_data[1].service_data[1].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Partidas Terminal Tupy*\n*Volta para Terminal Norte*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[1].stop_data[0].service_data[2].time_data[][].departure_time' | sort | uniq)
    MSG+="\n\n*Partidas Terminal Central*\n*Volta para Terminal Norte*\n*${DDS}:*\n"
    MSG+=$(cat Linhas/0040 | jq -r '.[1].stop_data[1].service_data[1].time_data[][].departure_time' | sort | uniq)

    ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
    --text "$(echo -e ${MSG})" --parse_mode markdown
}

Dias_func() {

    local DATA
    local TIME=$(date +%H)

    if [[ ${TIME} =~ (01|02) ]]; then
        ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
        --text '*Não há horários disponíveis*' --parse_mode markdown
        exit 1
    fi

    if [[ "${TIME}" = "00" ]]; then
        DATA=$(date -d yesterday +%a)
    else
        DATA=$(date +%a)
    fi

    case ${DATA} in
    seg | ter | qua | qui | sex)
        Dias_uteis_func
        ;;

    sáb)
        Sabados_func
        ;;

    dom)
        Domingos_func
        ;;
    *) ;;
    esac
}

Dias0040_func() {

    local DATA
    local TIME=$(date +%H)

    if [[ ${TIME} =~ (01|02) ]]; then
        ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
        --text '*Não há horários disponíveis*' --parse_mode markdown
        exit 1
    fi

    if [[ "${TIME}" = "00" ]]; then
        DATA=$(date -d yesterday +%a)
    else
        DATA=$(date +%a)
    fi

    case ${DATA} in
    seg | ter | qua | qui | sex)
        Uteis0040_func
        ;;

    sáb)
        Sabados0040_func
        ;;

    dom)
        Domingos0040_func
        ;;
    *) ;;
    esac
}

Validador_func() {

    local Linha
    local MSR

    MSR="*Linha Inexistente!*"

    Linha=$(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id)"')

    if grep -wq "${message_text}" <<<$Linha; then
        Dias_func
    else
        ShellBot.sendMessage --chat_id ${message_chat_id[$id]} \
        --text "$(echo -e ${MSR})" --parse_mode markdown
    fi
}

Verification_func() {

    if [[ ! -e Users/${message_chat_first_name}.txt ]]; then
        echo -e "\nUser: ${message_chat_first_name}\nData: $(date +%D)\nHora: $(date +%T)" >>Users/${message_chat_first_name}.txt
    fi

    if [[ ! -e Routers/router.txt ]]; then

        ./onibusinfo https://onibus.info/api/routes/group >>Routers/router.txt

        if [[ "${message_text}" = "0040" ]]; then
            Dias0040_func
        else
            Validador_func
        fi
    else
        if [[ "${message_text}" = "0040" ]]; then
            Dias0040_func
        else
            Validador_func
        fi
    fi
}

Verification_func
#=========================================[FIM]=========================================#