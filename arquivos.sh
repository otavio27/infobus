#!/usr/bin/env bash

Linhas_func() {

    local data=$(date +%d/%m/%Y)
    local hora=$(date +%H:%M)

    LINHAS=($(cat Routers/router.txt | jq -r '.[].routes[] | "\(.route_id)"'))

    for N in ${!LINHAS[@]}; do
        curl -X GET https://onibus.info/api/timetable/${LINHAS[$N]} \
        -H 'Accept: application/json, text/plain, /' \
        -H 'Accept-Encoding: gzip, deflate, br' \
        -H 'Accept-Language: en-US,en;q=0.5' \
        -H 'Cache-Control: no-cache' \
        -H 'Connection: keep-alive' \
        -H 'Cookie: _ga=GA1.2.1013964565.1582001733; _gid=GA1.2.528327386.1582001733; _gat=1' \
        -H 'Host: onibus.info' -H 'If-None-Match: W/"163-eOdyfXpHh2eKTCvQmg+OnnpiK/g"' \
        -H 'Postman-Token: cc8a1153-53ff-4a95-8d29-efdcc58ed728,777c9fe8-5073-4dee-805b-d1d2a9a28507' \
        -H 'Referer: https://onibus.info/api/timetable/${LINHAS[$N]}' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:68.0) Gecko/20100101 Firefox/68.0' \
        -H 'cache-control: no-cache' | gunzip >> Linhas/${LINHAS[$N]} 
    done

    echo -e "Arquivos de rotas e linhas atualizados em ${data} ás ${hora}" >> Arquivos/Update.txt
}

if [[ -e Routers/router.txt ]]; then
    rm Routers/router.txt
    rm Linhas/*
    curl -X GET https://onibus.info/api/routes/group \
    -H 'Accept: application/json, text/plain, /' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Accept-Language: en-US,en;q=0.5' \
    -H 'Cache-Control: no-cache' \
    -H 'Connection: keep-alive' \
    -H 'Cookie: _ga=GA1.2.1013964565.1582001733; _gid=GA1.2.528327386.1582001733; _gat=1' \
    -H 'Host: onibus.info' -H 'If-None-Match: W/"163-eOdyfXpHh2eKTCvQmg+OnnpiK/g"' \
    -H 'Postman-Token: cc8a1153-53ff-4a95-8d29-efdcc58ed728,777c9fe8-5073-4dee-805b-d1d2a9a28507' \
    -H 'Referer: https://onibus.info/api/routes/group' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:68.0) Gecko/20100101 Firefox/68.0' \
    -H 'cache-control: no-cache' | gunzip >> Routers/router.txt
    Linhas_func
else
    echo "Não foi possível atualizar o arquivo"
fi
